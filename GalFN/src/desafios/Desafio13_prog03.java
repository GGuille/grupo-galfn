package desafios;

import java.util.Scanner;

public class Desafio13_prog03 {

	public static void main(String[] args) {
		Scanner a = new Scanner(System.in);

		String matriz[][] = new String[3][3];

		int i = 0;
		// AGREGAR DATOS. DISCRIMINAMOS i 0 = nombre / 1 = dni / 2 = edad
		for (int j = 0; j < 3; j++) {
			if (i == 0) { // INGRESO NOMBRE
				System.out.print("Ingrese nombre: ");
				matriz[i][j] = a.next();
				System.out.print("Ingrese su DNI: ");
				matriz[i + 1][j] = a.next();
				System.out.print("Ingrese su edad: ");
				matriz[i + 2][j] = a.next();
				System.out.println();
			}
		}

		for (i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(matriz[i][j] + "	");
			}
			System.out.println();
		}

	}

}
